//
//  ModeSelectionViewController.swift
//  ZOTTZ_ui_tabbar
//
//  Created by Nayem BJIT on 8/30/17.
//  Copyright © 2017 Mufakkharul Islam Nayem. All rights reserved.
//

import UIKit

class ModeSelectionViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        //addButton(title: "Hello")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addButton(title: String) {
        
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        button.layer.cornerRadius = 0.5 * button.bounds.size.width
        button.clipsToBounds = true
//        button.setImage(UIImage(named:"thumbsUp.png"), for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        button.setTitle(title, for: .normal)
        button.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        button.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        view.addSubview(button)
        
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        
        print("\(sender.title(for: .normal)!)button pressed")
        
    }
    
}
