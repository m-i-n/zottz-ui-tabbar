//
//  PlaygroundViewController.swift
//  ZOTTZ_ui_tabbar
//
//  Created by Nayem on 8/31/17.
//  Copyright © 2017 Mufakkharul Islam Nayem. All rights reserved.
//

import UIKit

struct Equipment {
    let name: String
    let timesPressed: Int
}

class PlaygroundViewController: UIViewController {
    
    @IBOutlet weak var scissorDropDown: MKDropdownMenu!
    @IBOutlet weak var pencilDropDown: MKDropdownMenu!
    @IBOutlet weak var pincherDropDown: MKDropdownMenu!
    @IBOutlet weak var buttonDropDown: MKDropdownMenu!
    
    @IBOutlet weak var scissorMenuBackDropView: BackDropView!
    @IBOutlet weak var pencilMenuBackDropView: BackDropView!
    @IBOutlet weak var pincherMenuBackDropView: BackDropView!
    @IBOutlet weak var buttonMenuBackDropView: BackDropView!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    let colors = [ #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1), #colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 1), #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 1), #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1), #colorLiteral(red: 1, green: 0.2527923882, blue: 1, alpha: 1), #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1), #colorLiteral(red: 0, green: 0.9914394021, blue: 1, alpha: 1) ]
    let musics = ["Music 1", "Music 2", "Music 3", "Music 4"]
    var equipments: [Equipment] = []
    
    var colorComponentTitle: String?
    var musicComponentTitle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDropDownMenu(for: [scissorDropDown, pencilDropDown, pincherDropDown, buttonDropDown])
        setupBackDropView(for: [scissorMenuBackDropView, pencilMenuBackDropView, pincherMenuBackDropView, buttonMenuBackDropView])
        
        tableView.dataSource = self
        //        tableView.delegate = self
        equipments = dummyEquipmentData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func computedColorFor(row: Int)-> UIColor {
        //        let computedHue = CGFloat(row) / CGFloat(dropdownMenu.numberOfRows(inComponent: 0))
        //        return UIColor(hue: computedHue, saturation: 1.0, brightness: 1.0, alpha: 1.0)
        return colors[row]
    }
    
    func setupDropDownMenu(for views: [MKDropdownMenu]) {
        
        views.enumerated().forEach { (index, view) in
            view.tag = index
            view.dataSource = self
            view.delegate = self
            
            view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            view.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).cgColor
            view.layer.borderWidth = 0.5
            view.layer.cornerRadius = 10
            view.dropdownCornerRadius = 10
            view.dropdownRoundedCorners = [ .topLeft, .topRight, .bottomLeft, .bottomRight ]
            view.dropdownBackgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            
            view.dropdownShowsTopRowSeparator = false;
            view.dropdownShowsBottomRowSeparator = false;
            view.dropdownShowsBorder = true;
            
            view.backgroundDimmingOpacity = 0.05
        }
    }
    
    func setupBackDropView(for views: [BackDropView]) {
        
        views.forEach { (view) in
            view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            view.roundCorners([ .topLeft, .bottomLeft ], radius: 10)
        }
    }
    
    // TODO:- This method should be replaced with actual data
    func dummyEquipmentData() -> [Equipment] {
        
        let equipment1 = Equipment(name: "Scissor", timesPressed: 5)
        let equipment2 = Equipment(name: "Pencil", timesPressed: 15)
        let equipment3 = Equipment(name: "Pincher", timesPressed: 6)
        let equipment4 = Equipment(name: "Button", timesPressed: 2)
        
        return [equipment1, equipment2, equipment3, equipment4]
    }
    
    @IBAction func shareButtonPressed(_ sender: UIBarButtonItem) {
    }
}


extension PlaygroundViewController: MKDropdownMenuDataSource {
    func numberOfComponents(in dropdownMenu: MKDropdownMenu) -> Int {
        /// TODO: - have to return a value >= 0
        return 2
    }
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, numberOfRowsInComponent component: Int) -> Int {
        // TODO: - have to return a value >= 0
        switch component {
        case 0:
            return colors.count
        case 1:
            return musics.count
        default:
            return 0
        }
    }
}

extension PlaygroundViewController: MKDropdownMenuDelegate {
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, rowHeightForComponent component: Int) -> CGFloat {
        return 40.0
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, backgroundColorForRow row: Int, forComponent component: Int) -> UIColor? {
        // TODO: - have to return valid color
        switch component {
        case 0:
            let color = computedColorFor(row: row)
            return color
        default:
            let color = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            return color
        }
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, didSelectRow row: Int, inComponent component: Int) {
        
        switch component {
        case 0:
            colorComponentTitle = ""
            let selectedColor = colors[row]
            switch dropdownMenu.tag {
            case 0:
                scissorMenuBackDropView.backgroundColor = selectedColor
            case 1:
                pencilMenuBackDropView.backgroundColor = selectedColor
            case 2:
                pincherMenuBackDropView.backgroundColor = selectedColor
            case 3:
                buttonMenuBackDropView.backgroundColor = selectedColor
            default:
                break
            }
            dropdownMenu.reloadComponent(component)
        default:
            musicComponentTitle = musics[row]
            dropdownMenu.reloadComponent(component)
        }
        dropdownMenu.closeAllComponents(animated: true)
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, titleForComponent component: Int) -> String? {
        switch component {
        case 0:
            return colorComponentTitle ?? "Color"
        case 1:
            return musicComponentTitle ?? "Music"
        default:
            return nil
        }
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 1:
            return musics[row]
        default:
            return nil
        }
    }
    
}

extension PlaygroundViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return equipments.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Interaction Count"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let anEquipment = equipments[indexPath.row]
        cell.textLabel?.text = anEquipment.name
        cell.detailTextLabel?.text = String(anEquipment.timesPressed)
        
        return cell
    }
}

/*
 extension PlaygroundViewController: UITableViewDelegate {
 
 func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
 
 let frame = tableView.frame
 let buttonFrame = CGRect(x: frame.size.width - 60, y: 0, width: 50, height: 30)
 
 let shareButton = UIButton(type: .system)
 //        shareButton.setTitle("Share", for: .normal)
 shareButton.frame = buttonFrame
 shareButton.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
 
 let title = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
 title.text = "Section Header"
 
 let headerView = UIView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height) )
 
 //        headerView.addSubview(title)
 headerView.addSubview(shareButton)
 
 return headerView
 }
 
 }
 */
