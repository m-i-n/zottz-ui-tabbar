//
//  HomeViewController.swift
//  ZOTTZ_ui_tabbar
//
//  Created by Nayem BJIT on 8/31/17.
//  Copyright © 2017 Mufakkharul Islam Nayem. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBluetoothIndicator()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupBluetoothIndicator() {
        
        let indicatorFrame = CGRect(x: view.frame.width * 0.5 - 100.0, y: view.frame.height * 0.5 - 100.0, width: 200.0, height: 200.0)
        
        // .ballScaleMultiple & .ballScale both type fits with this use case
        let activityIndicator = NVActivityIndicatorView(frame: indicatorFrame, type: .ballScaleMultiple, color: #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 1), padding: nil)
        activityIndicator.autoresizingMask = [ .flexibleTopMargin, .flexibleBottomMargin, .flexibleLeftMargin, .flexibleRightMargin ]
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        let imageViewFrame = CGRect(x: view.frame.width * 0.5 - 50.0, y: view.frame.height * 0.5 - 50.0, width: 100.0, height: 100.0)
        let imageView = UIImageView(frame: imageViewFrame)
        imageView.contentMode = .scaleAspectFit
        imageView.image = #imageLiteral(resourceName: "Bluetooth")
        imageView.autoresizingMask = [ .flexibleTopMargin, .flexibleBottomMargin, .flexibleLeftMargin, .flexibleRightMargin ]
        view.addSubview(imageView)
        
    }
    
}
